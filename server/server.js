const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const PORT = process.env.API_PORT

// Use bluebird
mongoose.Promise = require('bluebird')
// assert.equal(query.exec().constructor, require('bluebird'))

const app = express()
app.set('view engine', 'html')

// Body Parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

// const router = express.Router()
// router.get('/', (req, res) => {
//   res.sendStatus(200)
// })
app.use('/users', require('./routes/users'))
app.use('/bases', require('./routes/bases'))

///////////////// Error handlers //////////////////

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found')
//   err.status = 404
//   res.sendStatus(404)
// })

// development error handler will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(500).send({
      message: err.message,
      error: err
    })
  })
}

// production error handler no stacktraces leak to user
app.use(function(err, req, res, next) {
  res.status(500).send({
    message: err.message,
    error: {}
  })
})

//////////// Connect to DB & run app ///////////////

mongoose.connect('mongodb://mongodb:27017', {
  useMongoClient: true,
  autoIndex: false,
})
  .then(() => {
    app.listen(PORT, function() {
      console.log(`Server is listening on port ${PORT}`)
    })
  })
