const mongoose = require('mongoose')

const Device = new mongoose.Schema({
  platform: String,
  browser: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  firstLogin: {
    type: Date,
    default: Date.now,
  },
  lastLogin: {
    type: Date,
    default: Date.now,
  },
}, { versionKey: false })

module.exports = Device