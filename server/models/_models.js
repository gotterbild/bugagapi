const mongoose = require('mongoose')

module.exports = {
	User: mongoose.model('User', require('./User')),
	Device: mongoose.model('Device', require('./Device')),
	Base: mongoose.model('Base', require('./Base')),
	Currency: mongoose.model('Currency', require('./Currency')),
	History: mongoose.model('History', require('./History')),
	CategoryGroup: mongoose.model('CategoryGroup', require('./CategoryGroup')),
	Category: mongoose.model('Category', require('./Category')),
	Record: mongoose.model('Record', require('./Record')),
}