const mongoose = require('mongoose')

const Currency = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  icon: String,
  code: {
    type: String,
    required: true,
    trim: true,
  },
  sign: {
    type: String,
    required: true,
    trim: true,
  },
  defaultRate: {
    type: Number,
    required: true,
  },
  defaultCurrency: Boolean,

  base: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Base',
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  history: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'History',
  }],
}, { versionKey: false })

Currency.virtual('predefined').get(function () {
  const predefinedCurrencies = ['USD', 'EUR', 'GBP', 'CNY', 'RUB']

  for (let currency of predefinedCurrencies) {
    if (currency === this.code)
      return true
  }

  return false
});

module.exports = Currency