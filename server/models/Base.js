const mongoose = require('mongoose')
const CategoryGroup = require('./CategoryGroup')
const Record = require('./Record')
const Currency = require('./Currency')

const Base = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  color: {
    type: String,
    required: true,
  },
  currencies: [Currency],
  categories: [CategoryGroup],
  records: [Record],

  admin: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  guests: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    email: String,
    rights: {
      addRecords: false,
      editRecords: false,
      deleteRecords: false,
      addCategories: false,
      editCategories: false,
      deleteCategories: false,
      readGuests: false,
      addGuests: false,
      removeGuests: false,
    }
  }],
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  history: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'History',
    selected: false,
  },
}, { versionKey: false })

module.exports = Base