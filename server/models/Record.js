const mongoose = require('mongoose')
const Currency = require('./Currency')

const Record = new mongoose.Schema({
  hash: String,
  name: {
    type: String,
    required: true,
    trim: true,
  },
  price: {
    type: Number,
    required: true,
  },
  expression: String,
  currency: Currency,
  date: {
    type: Date,
    required: true,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
  },
  location: {
    lat: Number,
    lon: Number,
  },

  source: String, // app(web, android, ios), bot, alisa, sms(phone num)
  base: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Base',
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  history: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'History',
  }],
}, { versionKey: false })

module.exports = Record