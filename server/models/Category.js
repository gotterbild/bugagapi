const mongoose = require('mongoose')

const Category = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  color: {
    type: String,
    required: true,
  },
  group: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CategoryGroup',
    required: true,
  },
  icon: String,
  records: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Record',
  }],

  base: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Base',
    selected: false,
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  dateStart: Date,
  dateFinish: Date,
  history: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'History',
    selected: false,
  }],
}, { versionKey: false })

module.exports = Category