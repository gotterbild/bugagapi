const mongoose = require('mongoose')

const User = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  emailVerified: Boolean,
  password: {
    type: String,
    required: true,
    select: false,
    minlength: 6,
  },
  bases: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Base',
  }],
  devices: {
    type: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Device',
    }],
    select: false,
  },

  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
}, { versionKey: false })

module.exports = User