const mongoose = require('mongoose')
const Category = require('./Category')

const CategoryGroup = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },
  categories: [Category],
  base: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Base',
  },

  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  history: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'History',
    selected: false,
  },
}, { versionKey: false })

module.exports = CategoryGroup