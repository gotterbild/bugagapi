const express = require('express')
const router = express.Router()
const performRequest = require('../helpers/performRequest')
const UsersController = require('../controllers/users/_UsersController')

router.route('/')
  .get((req, res) => performRequest(req, res, 'User', 'getUser', true))
  .post((req, res) => performRequest(req, res, 'User', 'createUser'))
  .delete((req, res) => performRequest(req, res, 'User', 'deleteUser', true))
  .patch((req, res) => performRequest(req, res, 'User', 'updateUser', true))

router.route('/all')
  .get((req, res) => performRequest(req, res, 'User', 'getAllUsers'))

router.route('/login')
  .post((req, res) => performRequest(req, res, 'User', 'loginUser'))

module.exports = router