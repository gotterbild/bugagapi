const express = require('express')
const router = express.Router()
const BasesController = require('../controllers/bases/_BasesController')
const CategoriesController = require('../controllers/categories/_CategoriesController')

router.route('/')
  .get(BasesController.getAllBases)
  .post(BasesController.createBase)

router.route('/:baseId')
  .get(BasesController.getBase)
  .delete(BasesController.deleteBase)
  .patch(BasesController.updateBase)

router.route('/:baseId/category')
  .post(CategoriesController.createCategory)

router.route('/:baseId/guest')
  .post(CategoriesController.createCategory)

module.exports = router
