const Base = require('../../models/_models').Base
const Category = require('../../models/_models').Category
const checkAuthorization = require('../../helpers/checkAuthorization')

function createCategory(req, res, next) {
  const userId = checkAuthorization(req, res)
  if (userId) {
    const baseId = req.params.baseId
    Base.findById(baseId).select('admin guests')
      .then((base) => {
        const userIsBaseAdmin = String(userId) === String(base.admin)
        if (userIsBaseAdmin) {
          res.sendStatus(200)
        }else{
          res.sendStatus(403)
        }
      })
      .catch((err) => {
        console.error('Category Create base not found:', err)
        res.sendStatus(404)
      })

    // const category = new Category({
    //   name: req.body.name,
    //   color: req.body.color,
    //   icon: req.body.icon,
    //   admin: userId,
    //   base: baseId,
    // })

    // category.save((err, category) => {
    //   if (err) {
    //     console.error('Category Create error:', err)
    //     res.sendStatus(500)
    //   }else{
    //     Base.update(
    //       { _id: baseId },
    //       { '$addToSet': {'categories':  category._id } }
    //     )
    //       .then((user) => {
    //         res.status(201).send(category)
    //       })
    //       .catch(() => {
    //         res.sendStatus(510) // TODO Create an endpoint to send bug messages to
    //       })
    //   }
    // })
  }
}

module.exports = createCategory
