const User = require('../../models/_models').User
const Base = require('../../models/_models').Base
const checkAuthorization = require('../../helpers/checkAuthorization')

function getAllBases(req, res, next) {
  console.log('req', req)
  const authorizedUserId = checkAuthorization(req, res)
  if (authorizedUserId) {
    User.findById(authorizedUserId)
      .then((user) => {
        Base
          .find({ '_id': { $in: user.bases } })
          .select('name color records admin created')
          .then((bases) => {
            res.send(bases)
          })
      })
      .catch((err) => {
        res.sendStatus(404)
      })
  }
}

module.exports = getAllBases