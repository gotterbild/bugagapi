const Base = require('../../models/_models').Base
const checkAuthorization = require('../../helpers/checkAuthorization')

function updateBase(req, res, next) {
  const authorizedUserId = checkAuthorization(req, res)
  if (authorizedUserId) {
    const BaseId = req.params.baseId

    Base.findById(BaseId)
      .then((base) => {
        if (base) {
          const isBaseAdmin = String(authorizedUserId) === String(base.admin)
          if (isBaseAdmin) {
            base.name = req.body.name
            base.color = req.body.color
            base.updated = Date.now()
            base.save((err, base) => {
              if (err)
                res.sendStatus(500)
              else
                res.status(200).send(base)
            })
          }else{
            res.sendStatus(403)
          }
        }else{
          res.sendStatus(404)
        }
      })
      .catch((err) => {
        console.log('err', err)
        res.sendStatus(500)
      })

    Base.update({_id: BaseId},

    )
  }else{
    res.sendStatus(401)
  }
}

module.exports = updateBase