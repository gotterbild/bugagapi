module.exports = {
  createBase:  require('./createBase'),
  deleteBase:  require('./deleteBase'),
  getAllBases: require('./getAllBases'),
  getBase:     require('./getBase'),
  updateBase:  require('./updateBase'),
}