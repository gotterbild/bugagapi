const User = require('../../models/_models').User
const Base = require('../../models/_models').Base
const checkAuthorization = require('../../helpers/checkAuthorization')

function addGuestToBase(req, res, next) {
  const userId = checkAuthorization(req, res)
  if (userId) {
    const BaseId = req.params.baseId

    Base.findById(BaseId)
      .then((base) => {
        const userIsBaseAdmin = (String(userId) === String(base.admin))
        if (userIsBaseAdmin) {
          res.sendStatus(200)
        }else{
          res.sendStatus(403)
        }
        // base.update()
      })
      .catch((err) => {
        console.log('Add Guest to Base err:', err)
        res.sendStatus(404)
      })

    // base.save((err, base) => {
    //   if (err) {
    //     console.error('Base Create error:', err)
    //     res.sendStatus(500)
    //   }else{
    //     User.update(
    //       { _id: userId },
    //       { '$addToSet': {'bases':  base._id } }
    //     )
    //       .then((user) => {
    //         res.status(201).send(base)
    //       })
    //       .catch(() => {
    //         res.sendStatus(510) // TODO Create an endpoint to send bug messages to
    //         // TODO In this case client should make request to bugs endpoint
    //       })
    //   }
    // })
  }
}

module.exports = addGuestToBase
