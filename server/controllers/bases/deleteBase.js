const User = require('../../models/_models').User
const Base = require('../../models/_models').Base
const checkAuthorization = require('../../helpers/checkAuthorization')

function deleteBase(req, res, next) {
  const authorizedUserId = checkAuthorization(req, res)
  if (authorizedUserId) {
    const BaseId = req.params.baseId
    Base.findById(BaseId)
      .then((base) => {
        if (base) {
          if (String(authorizedUserId) === String(base.admin)) {
            base.remove((data) => {
              removeBaseFromUser(authorizedUserId, BaseId, res)
                .then(() => {
                  res.sendStatus(200)
                })
                .catch((err) => {
                  res.sendStatus(510) // TODO Create a tableto send bug messages to
                })
            })
          }else{
            res.sendStatus(403)
          }
        }else{
          removeBaseFromUser(BaseId, res)
            .then(() => {
              res.sendStatus(404)
            })
            .catch(() => {
              res.sendStatus(510)
            })
        }
      })
      .catch((err) => {
        console.log('err', err)
        res.sendStatus(404)
      })
  }else{
    res.sendStatus(401)
  }
}

function removeBaseFromUser(authorizedUserId, BaseId, res) {
  return User.update(
    { _id: authorizedUserId },
    { '$pull': {'bases':  BaseId } }
  )
}

module.exports = deleteBase
