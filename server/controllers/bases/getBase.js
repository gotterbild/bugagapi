const User = require('../../models/_models').User
const Base = require('../../models/_models').Base
const checkAuthorization = require('../../helpers/checkAuthorization')

function getBase(req, res, next) {
  const userId = checkAuthorization(req, res)
  if (userId) {
    const baseId = req.params.baseId
    Base.findById(baseId)
      .then((base) => {
        const hasPermissions = String(userId) === String(base.admin)
        // TODO: add check for guest
        if (hasPermissions) {
          res.send(base)
        }else{
          res.sendStatus(403)
        }
      })
      .catch((err) => {
        console.log('Base Get error:', err)
        res.sendStatus(404)
      })

    // // Alternatively search user with a base in list of bases

    // User.find({ _id: userId, bases: { $in: [req.params.baseId] } })
    //   .then((user) => {
    //     console.log('Get Base user then', user)
    //     Base.findById(baseId)
    //       .then((base) => {
    //         res.send(base)
    //       })
    //       .catch((err) => {
    //         console.log('Base Get error:', err)
    //         res.sendStatus(404)
    //       })
    //   })
    //   .catch((err) => {
    //     console.log('Get Base user catch', err)
    //     res.sendStatus(403)
    //   })
  }
}

module.exports = getBase