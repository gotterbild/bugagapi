const User = require('../../models/_models').User
const Base = require('../../models/_models').Base
const checkAuthorization = require('../../helpers/checkAuthorization')

function createBase(req, res, next) {
  const authorizedUserId = checkAuthorization(req, res)
  if (authorizedUserId) {

    const base = new Base({
      name: req.body.name,
      color: req.body.color,
      admin: authorizedUserId,
      guests: [],
    })

    base.save((err, base) => {
      if (err) {
        console.error('Base Create error:', err)
        res.sendStatus(500)
      }else{
        User.update(
          { _id: authorizedUserId },
          { '$addToSet': {'bases':  base._id } }
        )
          .then((user) => {
            res.status(201).send(base)
          })
          .catch(() => {
            res.sendStatus(510) // TODO Create an endpoint to send bug messages to
            // TODO In this case client should make request to bugs endpoint
          })
      }
    })
  }else{
    res.sendStatus(401)
  }
}

module.exports = createBase
