const User = require('../../models/_models').User
const createToken = require('../../helpers/createToken')
const hashPassword = require('../../helpers/hashPassword')

function createUser(params, data) {
  return new Promise((resolve, reject) => {
    const user = new User({
      email: data.email,
      password: hashPassword(data.password),
    })

    user.save()
      .then((user) => {
        const token = createToken(user._id)
        resolve({
          statusCode: 201,
          data: token,
        })
      })
      .catch((err) => reject(500))
  })
}

module.exports = createUser
