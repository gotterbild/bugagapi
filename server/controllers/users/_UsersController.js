module.exports = {
  createUser:  require('./createUser'),
  deleteUser:  require('./deleteUser'),
  getAllUsers: require('./getAllUsers'),
  getUser:     require('./getUser'),
  loginUser:   require('./loginUser'),
  updateUser:  require('./updateUser'),
}