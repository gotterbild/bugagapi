const User = require('../../models/_models').User

function getUser(id) {
  return new Promise((resolve, reject) => {
    User.findById(id)
      .then((user) => resolve(user))
      .catch((err) => reject(404))
  })
}

module.exports = getUser