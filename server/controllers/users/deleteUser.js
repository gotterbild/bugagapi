const User = require('../../models/_models').User

function deleteUser(userId) {
  return new Promise((resolve, reject) => {
    User.findById(userId)
      .then((user) => {
        if (user) {
          user.remove((data) => {
            resolve(200)
          })
        }else{
          reject(404)
        }
      })
      .catch((err) => {
        reject(404)
      })
  })
}

module.exports = deleteUser
