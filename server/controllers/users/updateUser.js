const User = require('../../models/_models').User

function updateUser(userId, params, data) {
  return new Promise((resolve, reject) => {
    User.findById(userId)
      .then((user) => {
        if (user) {
          user.email = data.email
          user.bases = data.bases
          user.updated = Date.now()
          user.save()
            .then((user) => {
              resolve(user)
            })
            .catch((err) => reject(409))
        }else{
          reject(404)
        }
      })
      .catch((err) => reject(404))
  })
}

module.exports = updateUser