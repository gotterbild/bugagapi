const dev = process.env.DEVELOPMENT === 'true'
const User = require('../../models/_models').User

function getAllUsers() {
  return new Promise((resolve, reject) => {
    if (dev) {
      User.find()
        .then((users) => resolve(users))
        .catch((err) => reject(err))
    }else{
      reject(401)
    }
  })
}

module.exports = getAllUsers