const User = require('../../models/_models').User
const createToken = require('../../helpers/createToken')
const hashPassword = require('../../helpers/hashPassword')

function loginUser(params, data) {
  return new Promise((resolve, reject) => {
    User.findOne({ email: data.email })
      .select(['_id', 'password'])
      .then((user) => {
        const incorrectMessage = 'Incorrect email or password'
        if (hashPassword(data.password) === user.password) {
          const token = createToken(user.id)
          resolve(token)
        }else{
          reject(406)
        }
      })
      .catch((err) => reject(406))
  })
}

module.exports = loginUser
