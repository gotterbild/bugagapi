/*
  Function for handling sequelize validation errors

  validationObject exmaple:
  {
    email: {
      isEmail: [406, 'Incorrect email'],
      not_unique: [409, 'Email already in use'],
    },
    password: {
      notIn: [406, 'Weak password'],
      len: [406, 'Password must be between 6 and 256 characters'],
    }
  }
*/

const sendError = require('../helpers/sendError')
const dev = process.env.DEVELOPMENT === 'true'

function handleError(res, err, validationObject) {
  let code = 500
  let message = 'Unhandled error'

  if (err && err.errors && err.errors[0]) {
    const field = err.errors[0].path
    const validatorKey = err.errors[0].validatorKey

    console.log('handleError field', field, 'key', validatorKey)
    if (validatorKey === 'is_null') {
      code = 406
      message = `${field} can not be empty`
    }else{
      if (validationObject) {
        code = validationObject[field][validatorKey][0]
        message = validationObject[field][validatorKey][1]
      }
    }
  }

  if (dev) {
    console.log(err)
  }

  sendError(res, code, message)
  return false
}

module.exports = handleError