function sendError(res, code, message) {
	res.status(code)
	res.send(message)
	res.end()
}

module.exports = sendError