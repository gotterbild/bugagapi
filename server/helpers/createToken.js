const jwt = require('jsonwebtoken')
const tokenSecret = process.env.TOKEN_SECRET

function createToken(id) {
  const token = jwt.sign({ id: id }, tokenSecret)
  return token
}

module.exports = createToken