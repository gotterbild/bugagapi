const verifyToken = require('./verifyToken')

function checkAuthorization(req, res) {
  const token = req.headers.authorization
  if (token) {
    const id = verifyToken(token)
    if (id) {
      return id
    }else{
      return false
    }
  }else{
    return false
  }
}

module.exports = checkAuthorization