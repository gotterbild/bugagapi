const crypto = require('crypto')
const salt = process.env.PASSWORD_SALT

function hashPassword(password) {
  return crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex')
}

module.exports = hashPassword