const jwt = require('jsonwebtoken')
const tokenSecret = process.env.TOKEN_SECRET

function verifyToken(token) {
  return jwt.verify(token, tokenSecret, function(err, decoded) {
    if (err) {
      return false
    }
    if (decoded) {
      const expiresIn = 604800 // 1 week without milliseconds
      const now = Math.ceil( Number(Date.now()) / 1000) // trim milliseconds to meet jsonwebtoken lib format
      const expires = decoded.iat + expiresIn
      const expired = expires < now
      if (expired) {
        return false
      }else{
        return decoded.id
      }
    }
  })
}

module.exports = verifyToken