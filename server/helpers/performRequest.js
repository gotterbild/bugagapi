const dev = process.env.DEVELOPMENT === 'true'
const checkAuthorization = require('./checkAuthorization')
const controllers = {
  User: require('../controllers/users/_UsersController'),
}

function performRequest(req, res, controller, method, useAuthorization) {
  if (useAuthorization) {
    const userId = checkAuthorization(req, res)
    if (userId) {
      controllers[controller][method](userId, req.params, req.body)
        .then((data) => handleSuccess(data))
        .catch((err) => handleError(err))
    }else{
      handleError(err)
    }
  }else{
    controllers[controller][method](req.params, req.body)
      .then((data) => handleSuccess(data))
      .catch((err) => handleError(err))
  }

  function handleSuccess(data) {
    if (data.statusCode) {
      res
        .status(data.statusCode)
        .send(data.data)
    }else{
      res.send(data)
    }
  }

  function handleError(err) {
    if (typeof err === 'number') {
      res.sendStatus(err)
    }else{
      if (err.code && err.message){
        res
          .status(code)
          .send(message)
          .end()
      }else{
        if (dev)
          res.send(err)
        else
          res.sendStatus(500)
      }
    }
  }
}

module.exports = performRequest